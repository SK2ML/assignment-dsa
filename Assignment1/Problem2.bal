import ballerina/graphql;

public type CovidStats record {|
    readonly string city;
    string date;
    string region;
    int deaths;
    int confirmed_cases;
    int recoveries;
    int tested;
|};

//table
isolated table<CovidStats> key(city) covidStatsTable = table [
{city: "WHK", date: "12/09/2021", region: "Khomas", deaths: 39, confirmed_cases: 465, recoveries: 67, tested: 1200}
];

isolated distinct service class data {

    private final readonly & CovidStats records;

    isolated function init(CovidStats records) {

        self.records = records.cloneReadOnly();

    }

    //function to get city
    isolated resource function get city() returns string {
        lock {
            return self.records.city;
        }
    }

    //function to get date
    isolated resource function get date() returns string {
        lock {

            return self.records.date;
        }
    }
    //function to get region
    isolated resource function get region() returns string {
        lock {
            return self.records.region;
        }
    }

    //function to get deaths 
    isolated resource function get deaths() returns int? {
        lock {
            return self.records.deaths;
        }
    }

    //function to get reecoveries
    isolated resource function get recovered() returns int? {

        lock {
            return self.records.recoveries;
        }
    }

    //function to get test cases
    isolated resource function get tested() returns int? {

        lock {
            return self.records.tested;
        }
    }

    //function to get confirmed cases
    isolated resource function get confirmed_cases() returns int? {
        lock {
            return self.records.confirmed_cases;
        }
    }

}

service /covid19 on new graphql:Listener(8080) { // service implementation

    isolated resource function get all() returns data[] {

        lock {
            CovidStats[] records = covidStatsTable.clone().toArray().cloneReadOnly();
            return records.clone().map(rec => new data(rec));
        }
    }

    isolated resource function get filter(string city) returns data? {

        lock {
            CovidStats? covidEntry = covidStatsTable[city].clone();
            if covidEntry is CovidStats {
                return new (covidEntry);
            }
            return;
        }
    }

    isolated remote function add(CovidStats rec) returns data? {

        lock {
            covidStatsTable.clone().add(rec.clone());
            return new data(rec.clone());
        }
    }
}

