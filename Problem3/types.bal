public type StudentRecord record {
    string studentNumber;
    string name;
    string emailAdress;
    string course;
    string courseCode;
    string assessment;
    float marks;
};

public type ErrorMsg record {
    string errorMsg;
};
