import ballerina/http;





// define the student record

type StudentRecord record {|

    readonly string studentNumber;
    string name ;
    string emailAdress;
    string course;
    string courseCode;
    string assessment;
    float marks;

|};


// error message
type ConflictingStudentNumberError record {|

 *http:Conflict;
  ErrorMsg body;

|};

type ErrorMsg record {|

 string errorMsg;
|};


// 


type StudentRecordCreated record {|
*http:Created;
StudentRecord[] body;

|};



//message error when request not found

type StudentRecordNotFound record {|

*http:NotFound;
string errMsg;

|};




// Student table

 isolated table <StudentRecord> key (studentNumber) studentTable = table[

//just for ensuring that the Table works perefctly let insert manually some data ;|||

  {studentNumber : "220043906", name: "Kanyinda Samuel Kanyinda",  emailAdress: "220043906@Student.nust.na", course: "Computer Science SD",courseCode: "DSA611S", assessment: "DSA", marks: 60}

 ];




isolated service /Nust/StudentsRecord on new http:Listener(9000){

    //create new student rec.

      isolated resource function post studentsIn(@http:Payload StudentRecord[] studentEntries) 
      returns StudentRecordCreated|ConflictingStudentNumberError {

           lock{
                string[] conflictingStudentNumb = from StudentRecord studentRegistered in studentEntries.clone()

                 where studentTable.clone().hasKey(studentRegistered.studentNumber) 

                 select studentRegistered.studentNumber;


                 if conflictingStudentNumb.length() >0 {


                        return <ConflictingStudentNumberError>{
                            body:{

                                // errMsg: 'join("", "There was an Error Pls check the following : ",...conflictingStudentNumb)
                               errorMsg: "Error! "


                            }
                        };

                 }
                 else {

                     studentEntries.clone().forEach( studentRegistered => studentTable.clone().add( studentRegistered));
                     return <StudentRecordCreated> {
                        body:studentEntries.clone()};
                     }
                 
                 }

           }


      }




isolated function 'join(string s, string s1, any c) returns string {
    return "";
}





